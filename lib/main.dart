import 'package:flutter/material.dart';
import 'package:transportdriver/services/localStorage_service.dart';
import 'package:transportdriver/pages/signin_page.dart';
import 'package:transportdriver/pages/home_page.dart';
import 'package:transportdriver/services/service_locator.dart';

//void main() => runApp(new MyApp());
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Center(
      child: new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Client Driver',
        theme: new ThemeData(
          primaryColor: Colors.white,
          canvasColor: Colors.white,
          accentColor: Color.fromRGBO(128, 161, 212, 1)
        ),
        home: _getStartUpScreen(),
      ),
    );
  }

  Widget _getStartUpScreen() {
   var localStorageService = locator<LocalStorageService>();
    if(!localStorageService.hasLoggedIn) {
      return SignInPage();
    }
    return HomePage();
  }

}
