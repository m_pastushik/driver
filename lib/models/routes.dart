import 'package:transportdriver/models/stops.dart';

class Route {
  List<Routes> routes;

  Route({this.routes});

  Route.fromJson(Map<String, dynamic> json) {
    if (json['routes'] != null) {
      routes = new List<Routes>();
      json['routes'].forEach((v) {
        routes.add(new Routes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.routes != null) {
      data['routes'] = this.routes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Routes {
  String id;
  String routeName;
  List<Stops> stops;

  Routes({this.id, this.routeName, this.stops});

  Routes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    routeName = json['routeName'];
    if (json['stops'] != null) {
      stops = new List<Stops>();
      json['stops'].forEach((v) {
        stops.add(new Stops.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['routeName'] = this.routeName;
    if (this.stops != null) {
      data['stops'] = this.stops.map((v) => v.toJson()).toList();
    }
    return data;
  }
}