import 'package:transportdriver/models/coordinates.dart';

class Stops {
  String id;
  String stopName;
  LatLng latLng;
  int inNum;
  int outNum;
  Stops({this.stopName, this.latLng, this.inNum, this.outNum});

  Stops.fromJson(Map<String, dynamic> json) {
    stopName = json['stopName'];
    latLng =
    json['latLng'] != null ? new LatLng.fromJson(json['latLng']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['stopName'] = this.stopName;
    if (this.latLng != null) {
      data['latLng'] = this.latLng.toJson();
    }
    return data;
  }
}
