class RouteRequest{
  int routeId;
  int startId;
  int endId;

  RouteRequest({this.routeId, this.startId, this.endId});
  RouteRequest.fromJson(Map<String, dynamic> json){
    routeId = json['routeId'];
    startId = json['startStopId'];
    endId = json['endStopId'];
  }
}

class Request{
  int id;
  List<RouteRequest> routes;
  Request({this.id, this.routes});
  
  Request.fromJson(Map<String, dynamic> json){
    id = json['id'];
    if(json['routes'] != null){
      routes = new List<RouteRequest>();
      json['routes'].forEach((v){
        routes.add(new RouteRequest.fromJson(v));    
      });
    }
  }
  List<RouteRequest> getRequests(){
    return routes;
  }
  
}