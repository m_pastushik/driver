class LatLng {
  String latitude;
  String longtitude;

  LatLng({this.latitude, this.longtitude});

  LatLng.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longtitude = json['longtitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude;
    data['longtitude'] = this.longtitude;
    return data;
  }
}