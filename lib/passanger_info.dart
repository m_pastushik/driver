import 'models/request.dart';
import 'models/stops.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<Request> getData(int stopId) async{
  String url = 'http://localhost:8080/request/' + stopId.toString() + '/';
  final responce = await http.get(url);

  if(responce.statusCode == 200){
    return Request.fromJson(json.decode(responce.body));
  }
  else{
    throw Exception('Failed to load data');
  }
}

List<Stops> getPassNum(List<Stops> items, int i, int routeId){
  List<Stops> newData = items;
  Future<Request> data = getData(int.parse(items[i].id));
  List<RouteRequest> info;// = data.getRequests();
  info.forEach((idx) {
    if(idx.routeId == routeId) {
      if(idx.startId == int.parse(items[i].id)) items[i].inNum++;
      items.forEach((element) {
        if(int.parse(element.id) == idx.endId) {
          element.outNum++;
        }
      });
    }
    });
  return newData;
}

Future<bool> sendData(bool allPass) async{
  final http.Response response = await http.post(
    'http://localhost:8080/request/passNum',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, bool>{
      'title': allPass,
    }),
  );
  if (response.statusCode == 201) {
    return allPass;
  } else {
    throw Exception('Failed to load album');
  }
}