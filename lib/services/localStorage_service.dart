import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  static LocalStorageService _instance;
  static SharedPreferences _preferences;

  static const String LoggedInKey = 'loggedIn';

  bool get hasLoggedIn => _getFromDisk(LoggedInKey) ?? false;
  set hasLoggedIn(bool value) => _saveToDisk(LoggedInKey, value);

  dynamic _getFromDisk(String key) {
    var value  = _preferences.get(key);
    return value;
  }
  void _saveToDisk(String key, bool content){
    _preferences.setBool(LoggedInKey, content);
  }

  static Future<LocalStorageService> getInstance() async {
    if (_instance == null) {
      _instance = LocalStorageService();
    }
    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }
    return _instance;
  }
}