import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:transportdriver/models/driver.dart';

class AuthenticationService {
  Future<bool> register(Driver _driver) async {
    String url = 'http://localhost:8080/driver/username=${_driver.email}&password=${_driver.password}';
    http.Response response = await http.post(url);
    return response.statusCode == HttpStatus.ok;
  }

  Future<bool> login(Driver _driver) async {
    String url = 'http://localhost:8080/driver/username=${_driver.email}';
    http.Response response = await http.get(url);
    if(response.statusCode == HttpStatus.ok){
      if(_driver.password == response.body) return true;
      else throw Exception('Failed to login');
    }
  }

}