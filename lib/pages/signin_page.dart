import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:transportdriver/services/localStorage_service.dart';
import 'package:transportdriver/models/driver.dart';
import 'package:transportdriver/pages/home_page.dart';
import 'package:transportdriver/services/service_locator.dart';
import 'package:transportdriver/pages/registration_page.dart';
import 'package:transportdriver/validation.dart';

class SignInPage extends StatelessWidget {
  final _sizeTextBlack = const TextStyle(fontSize: 15.0, color: Colors.black);

  Driver _driver;
  String temp;
  String password;
  bool _autoValidate = false;
  final _formKey = GlobalKey<FormState>();

  void _submitCommand(BuildContext context) {
    final form = _formKey.currentState;
    if(form.validate()){
      // form.save();
      //if (signIn(_driver));
      _loginCommand(context);
    }
    else {
      _autoValidate = true;
    }
  }

  void _loginCommand(BuildContext context) {
    locator<LocalStorageService>().hasLoggedIn = true;
    Navigator.push(context, MaterialPageRoute(
        builder: (context) => HomePage()));
  }

  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'password is required'),
    MinLengthValidator(8, errorText: 'password must be at least 8 digits long'),
    PatternValidator(r'(?=.*?[#?!@$%^&*-_])', errorText: 'passwords must have at least one special character')
  ]);

  @override
  Widget build(BuildContext context) {
    double phoneHeight = MediaQuery.of(context).size.height;
    return Scaffold(
     resizeToAvoidBottomPadding: false,
     backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: phoneHeight/3),
                child: Text('ВХОД.',
                  style: TextStyle(fontSize: 30.0,
                    color: Color.fromRGBO(71, 68, 72, 1),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 50.0),
                child: TextFormField(
                  style: _sizeTextBlack,
                  onSaved: (val) => _driver.email = val,
                  validator: validateEmail,
                  decoration: InputDecoration(
                    labelText: "Почта",
                    hintText: "Почта",
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black54,
                      )
                    ),
                    prefixIcon: Icon(Icons.email),
                  ),
                ),
                width: 400.0,
              ),

              Container(
                padding: EdgeInsets.only(top: 25.0),
                child: TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Пароль',
                      hintText: 'Пароль',
                    prefixIcon: Icon(Icons.lock_outline),
                    focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black54,
                    )
                  )
                  ),
                  obscureText: true,
                  style: _sizeTextBlack,
                  onSaved: (val) => _driver.password = val,
                  validator: validatePassword,
                ),
                width: 400.0,
              ),
              Padding(
                  padding: EdgeInsets.only(top: 3.0),
                  child: FlatButton(
                    child: Text("Нет аккаунта? Создайте его тут.",
                      style: TextStyle(color: Color.fromRGBO(71, 68, 72, 1)),),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => RegistrationPage()
                      ));
                    },
                  )
              ),
              Container(
                width: 150.0,
                height: 90 ,
                child: Padding(
                    padding: EdgeInsets.only(top: 40.0),
                    child: RaisedButton (
                      child: Text('ВОЙТИ', style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        ),
                      ),
                      color: Color.fromRGBO(71, 68, 72, 1),
                      onPressed: () {
                        _submitCommand(context);
                      },
                    )
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}