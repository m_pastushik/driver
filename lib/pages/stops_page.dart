import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:transportdriver/models/stops.dart';
import 'package:transportdriver/pages/signin_page.dart';
import 'package:transportdriver/services/localStorage_service.dart';
import 'package:transportdriver/services/service_locator.dart';
import 'package:transportdriver/passanger_info.dart';
import 'home_page.dart';

class StopsPage extends StatefulWidget {
  final String name;
  final String routeId;
  final List<Stops> stops;
  StopsPage({Key key, @required this.name, @required this.stops, @required this.routeId}) : super(key: key);

  @override
  _StopsPageState createState() => _StopsPageState();
}

class _StopsPageState extends State<StopsPage>{
  String stopName;
  String routeId;
  List<Stops> stops = List<Stops>();
  List<Stops> itemsList = List<Stops>();

  void update(){
    for(int j = 0; j < 3; j++){
      itemsList = getPassNum(itemsList, j, int.parse(routeId));
    }
  }

  @override
  void initState() {
    stops.addAll(widget.stops);
    stopName = widget.name;
    routeId = widget.routeId;
    int i = 0;
    while (stopName != stops[i].stopName) i++;
    for(i; i < stops.length; i++){
      itemsList.add(stops[i]);
    }
    itemsList.add(Stops());
    super.initState();
   // update();
  }
  final _sizeTextBlack = const TextStyle(fontSize: 20.0, color: Colors.black);

  ListView generateItemsList() {
    double phoneWidth = MediaQuery.of(context).size.width;
    return ListView.builder(
      itemCount: itemsList.length-1,
      itemBuilder: (context, index) {
        return Dismissible(
          key: Key(itemsList[index].stopName),
          child: InkWell(
              child: Card(
                child: Container(
                  height: 200.0,
                  width: phoneWidth,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(50, 50, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            children: [
                              WidgetSpan(
                                child: Icon(Icons.directions_bus, size: 20, color: Color.fromRGBO(71, 68, 72, 1),),
                              ),
                              TextSpan(
                                text: '   Следующая остановка: ${itemsList[index].stopName}',
                                style: _sizeTextBlack
                              )
                            ]
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 20, 0, 3),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                WidgetSpan(
                                  child: Icon(Icons.arrow_upward, size: 20, color: Colors.greenAccent,)
                                ),
                                TextSpan(
                                  text:'   Пассажиров зайдет: $index',//itemsList[index].inNum
                                  style: _sizeTextBlack,
                                )
                              ]
                            ),
                          )
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 3),
                            child: RichText(
                              text: TextSpan(
                                  children: [
                                    WidgetSpan(
                                        child: Icon(Icons.arrow_downward, size: 20, color: Colors.redAccent,)
                                    ),
                                    TextSpan(
                                      text:'   Пассажиров выйдет: $index',//itemsList[index].outNum
                                      style: _sizeTextBlack,
                                    )
                                  ]
                              ),
                            )
                        )
                      ],
                    ),
                  ),
                ),
              )
          ),
          background: slideRightBackground(),
          secondaryBackground: slideLeftBackground(),
          // ignore: missing_return
          confirmDismiss: (direction) async {
            if (direction == DismissDirection.endToStart) {
              // ignore: missing_return
              //update();
              setState(() {
                itemsList.removeAt(index);
                //sendData(true);
                if(itemsList.length == 1){
                  return showEndDialog(context);
                }
              });
            } else {
              // ignore: missing_return
              //update();
              setState(() {
                itemsList.removeAt(index);
                //sendData(false);
                if(itemsList.length == 1){
                 return showEndDialog(context);
                }
              });
            }
          },
        );
      },
    );
  }

  showEndDialog(
    BuildContext context){
      Alert(context: context,
            title: "Конец",
            desc: "Вы достигли конечного пункта",
            buttons: [
              DialogButton(
                child: Text("В меню",  style: TextStyle(color: Colors.white, fontSize: 20)),
                color: Color.fromRGBO(71, 68, 72, 1),
                 onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomePage()))
              )
            ]).show();
    }

  Widget slideRightBackground() {
    return Container(
      color: Colors.green,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 20,
            ),
            Icon(
              Icons.edit,
              color: Colors.black,
            ),
            Text(
              " Все пассажиры",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
        alignment: Alignment.centerLeft,
      ),
    );
  }

  Widget slideLeftBackground() {
    return Container(
      color: Colors.red,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete,
              color: Colors.black,
            ),
            Text(
              " Остались пассажиры",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
        alignment: Alignment.centerRight,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.dehaze, color: Colors.white,),
            onPressed: (){
              locator<LocalStorageService>().hasLoggedIn = false;
              Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage()));
            },
          )
        ],
        leading: Container(),
        backgroundColor: Color.fromRGBO(71, 68, 72, 1),
      ),
      backgroundColor: Color.fromRGBO(71, 68, 72, 1),
      body: generateItemsList(),
    );
  }
}