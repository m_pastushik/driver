import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:transportdriver/models/stops.dart';
import 'package:transportdriver/pages/signin_page.dart';
import 'package:transportdriver/services/service_locator.dart';
import '../services/localStorage_service.dart';
import '../models/routes.dart';
import '../pages/stops_page.dart';
import 'package:transportdriver/data.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage>{
  TextEditingController editingController = TextEditingController();
  static var parsedJson = json.decode(someData)['routes'] as List;
  static List<Routes> routes = parsedJson.map((tagJson) => Routes.fromJson(tagJson)).toList();

  var items = List<Routes>();

  @override
  void initState() {
    items.addAll(routes);
    super.initState();
  }
  void filterSearchResults(String query) {
    List<Routes> dummySearchList = List<Routes>();
    dummySearchList.addAll(routes);
    if(query.isNotEmpty) {
      List<Routes> dummyListData = List<Routes>();
      dummySearchList.forEach((item) {
        if(item.routeName.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(routes);
      });
    }

  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Выберите нужный маршрут",
         style: TextStyle(color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Color.fromRGBO(71, 68, 72, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.dehaze, color: Colors.white,),
            onPressed: (){
              locator<LocalStorageService>().hasLoggedIn = false;
              Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage()));
            },
          )
        ],
        leading: Container(),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  filterSearchResults(value);
                },
                controller: editingController,
                decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25.0)))),
              ),
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return ListTile(
                      onTap: (){
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) =>
                                StopValid(stops: items[index].stops, route: items[index].routeName, routeId: items[index].id,))
                        );
                      },
                      title: Text('${items[index].routeName}', 
                      style: TextStyle(color: Color.fromRGBO(71, 68, 72, 1) ),),
                    );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class StopValid extends StatefulWidget {
  final List<Stops> stops;
  final String route;
  final String routeId;
  StopValid({Key key, @required this.stops, @required this.route, @required this.routeId}) : super (key: key);
  @override
  State<StatefulWidget> createState() {
    return _StopValidState();
  }
  //_StopValidState createState() => new _StopValidState();
}

class _StopValidState extends State<StopValid> {
  TextEditingController editingController = TextEditingController();
  List<Stops> stop = List<Stops>();
  List<Stops> items = List<Stops>();
  String routeName;
  String rId;
  @override
  void initState() {
    routeName = widget.route;
    rId = widget.routeId;
    stop.addAll(widget.stops);
    items.addAll(stop);
    super.initState();
  }

  void filterSearchResults(String query) {
    List<Stops> dummySearchList = List<Stops>();
    dummySearchList.addAll(stop);
    if (query.isNotEmpty) {
      List<Stops> dummyListData = List<Stops>();
      dummySearchList.forEach((item) {
        if (item.stopName.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(stop);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Выберите нужную остановку",
         style: TextStyle(color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Color.fromRGBO(71, 68, 72, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.dehaze, color: Colors.white,),
            onPressed: (){
              locator<LocalStorageService>().hasLoggedIn = false;
              Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage()));
            },
          )
        ],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  filterSearchResults(value);
                },
                controller: editingController,
                decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25.0)))),
              ),
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () {
                      Alert(
                        context: context, 
                        title: "Подтвердите ввод",
                        desc: "Маршрут: $routeName\nОстановка: ${items[index].stopName}",
                        style: AlertStyle(
                            titleStyle: TextStyle(color: Color.fromRGBO(71, 68, 82, 1),
                            fontWeight: FontWeight.bold),
                        ),
                        buttons: [
                          DialogButton(
                            child: Text("Продолжить",
                            style: TextStyle(color: Colors.white, fontSize: 18),),
                            color: Color.fromRGBO(71, 68, 72, 1),
                            onPressed: () => Navigator.push(context,
                                MaterialPageRoute(builder: (context) =>
                                  StopsPage(name: items[index].stopName, stops: stop, routeId: rId,))),
                          ),
                          DialogButton(
                            child: Text("Назад",
                              style: TextStyle(color: Colors.white, fontSize: 18),
                            ),
                            color: Color.fromRGBO(71, 68, 72, 1),
                            onPressed: () => Navigator.pop(context)
                          )
                        ]
                      ).show();
                    },
                    title: Text('${items[index].stopName}'),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
