import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:transportdriver/validation.dart';
import 'package:transportdriver/models/driver.dart';

class RegistrationPage extends StatelessWidget {
  final _sizeTextBlack = const TextStyle(fontSize: 15.0, color: Colors.black);
  bool _autoValidate = false;
  final _formKey = GlobalKey<FormState>();
  Driver _driver;
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();

  void _submitCommand(BuildContext context) {
    final form = _formKey.currentState;
    if(form.validate()){
      // form.save();
      //if(register(_driver));
      Navigator.pop(context);
    }
    else {
      _autoValidate = true;
    }
  }

  @override
  Widget build(BuildContext context){
    double phoneHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: phoneHeight/3),
                child: Text('РЕГИСТРАЦИЯ.',
                  style: TextStyle(fontSize: 30.0,
                    color: Color.fromRGBO(71, 68, 72, 1),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 50.0),
                child: TextFormField(
                  style: _sizeTextBlack,
                  onSaved: (val) => _driver.email = val,
                  validator: validateEmail,
                  decoration: InputDecoration(
                    labelText: "Почта",
                    hintText: "Почта",
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black54,
                      )
                    ),
                    prefixIcon: Icon(Icons.email),
                  ),
                ),
                width: 400.0,
              ),
              Container(
                padding: EdgeInsets.only(top: 25.0),
                child: TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Пароль',
                      hintText: 'Пароль',
                    prefixIcon: Icon(Icons.lock_outline),
                    focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black54,
                    )
                  )
                  ),
                  obscureText: true,
                  style: _sizeTextBlack,
                  controller: _pass,
                  validator: validatePassword,
                ),
                width: 400.0,
              ),
              Container(
                padding: EdgeInsets.only(top: 25.0),
                child: TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Повторите пароль',
                      hintText: 'Пароль',
                    prefixIcon: Icon(Icons.lock_outline),
                    focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black54,
                    )
                  )
                  ),
                  obscureText: true,
                  style: _sizeTextBlack,
                  controller: _confirmPass,
                  validator: (val) {
                    if(val != _confirmPass.text) return 'Пароли не совпадают';
                    else _driver.password = val;
                    return null;
                  },
                ),
                width: 400.0,
              ),
              Container(
                width: 150.0,
                height: 90 ,
                child: Padding(
                    padding: EdgeInsets.only(top: 40.0),
                    child: RaisedButton (
                      child: Text('ПРОДОЛЖИТЬ', style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        ),
                      ),
                      color: Color.fromRGBO(71, 68, 72, 1),
                      onPressed: () {
                        _submitCommand(context);
                      },
                    )
                ),
              ),
            ]
          ),
        )
      ),
    );
  }
}