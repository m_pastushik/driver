
String validateEmail(String value) {
    if (value.isEmpty) {
      return "Введите адрес электронной почты";
    }
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);
    if (regExp.hasMatch(value)) {
      return null;
    }
    return 'Неверный адрес';
}

String validatePassword(String value) {
  if (value.isEmpty){
     return 'Введите пароль';
  }
  if (value.length < 8) {
    return 'Слишком короткий пароль';
  }
  //^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[]:;<>,.?/~_+-=|\]).{8,32}$
  String p = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[~!@#$%^&*_-+=`|\(){}[]:;<>,.?/]).{8,}$';
  RegExp regExp = new RegExp(p);
  if (!regExp.hasMatch(value)) {
      return 'Пароль должен содержать одну заглавную букву, одну строчную и один специальный символ';
  }
  return null;
}

String checkPassword(String value, String pass){
  if(value != pass) 
    return 'Пароли не совпадают';
  return null;
}